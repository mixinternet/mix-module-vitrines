<?php
if (!isset($params)) {
    $params = [];
}

$params['id'] = 'VitrineCarousel';
$params['v-cloak'] = null;
$params['data-route-vitrines'] = $this->routeUrl('vitrines.index', ['language' => \I18n::$lang]);

if (isset($params['settings'])) {
    $params['settings'] = json_encode($params['settings']);
}

if (isset($params['player-vars'])) {
    $params['player-vars'] = json_encode($params['player-vars']);
}

echo new \Mix\Html\Tag('div', $params);
