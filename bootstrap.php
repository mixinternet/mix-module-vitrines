<?php
use Users\PermissionManager as PM;
use Users\Model\User;
use Vitrines\Controller\VitrinesController;

$permissoes = User::isAllowed('vitrines', 'index');

if ($permissoes) {
    $ui->addMenu(
        'vitrines',
        'Vitrines',
        'ellipsis-h',
        \URL::site(\Route::get('admin.crud')->uri(['config' => 'vitrines'])),
        null, 1);

    PM::getInstance()->addResource('vitrines', 'Vitrines');
}

\I18n\Route::set('vitrines.index', 'vitrines')
    ->defaults([
        'controller' => VitrinesController::class,
        'action'     => 'index'
    ]);
