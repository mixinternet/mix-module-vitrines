<?php
namespace Roboli\Task\Populate;

use Robo\Result;
use Vitrines\Model\Vitrine;
use Intervention\Image\ImageManagerStatic as Image;

class Vitrines extends \Roboli\AbstractPopulate
{
    /**
     * {@inheritdoc}
     */
    public function run()
    {
        $table = Vitrine::getTable();

        $this->info("Generating fake data for $table");

        $connection = self::getConnection('production');
        $connection->beginTransaction();
        for($i = 0; $i <= $this->_numItems; $i++) {
            /** @var Doctrine\DBAL\Query\QueryBuilder */
            $query = $connection->createQueryBuilder();
            $query->insert($table);

            $backgroundColor = $this->faker()->hexcolor;
            $image = $this->_createImage(Vitrine::UPLOAD_DIR, Vitrine::getImageSizes(), $backgroundColor);
            $titulo = $this->faker()->sentence(5);

            $data = [
                'titulo' => $query->createNamedParameter($titulo),
                'imagem' => $query->createNamedParameter($image),
                'cor' => $query->createNamedParameter($backgroundColor),
                'ordem' => $query->createNamedParameter(
                    $this->faker()->numberBetween(1, $this->_numItems)
                ),
                'data_cadastro' => 'NOW()'
            ];

            if ($this->faker()->boolean(30)) {
                $data['data_comeca'] = $query->createNamedParameter(
                    $this->faker()->dateTimeBetween('now', '+2 years')->format('Y-m-d H:i:s')
                );
            }

            if ($this->faker()->boolean(30)) {
                $data['ativa'] = $query->createNamedParameter('0');
            }

            if ($this->faker()->boolean(30)) {
                $data['url'] = $query->createNamedParameter($this->faker()->url);
                $data['nova_aba'] = $query->createNamedParameter(true);
            }

            try {
                $query->values($data)->execute();
            }
            catch (\Doctrine\DBAL\Exception\UniqueConstraintViolationException $exception) {
                $this->warning('Record already inserted');
            }
        }
        $connection->commit();

        $this->success("Table $table was successfully populated with " . $this->_numItems . " items");

        parent::afterRun();

        return Result::success($this, "Everyting is fine");
    }
}
