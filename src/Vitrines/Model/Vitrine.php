<?php

namespace Vitrines\Model;

use Application\Model\BaseModel;
use Mix\Model\Field\Validation\FileMaxSize;
use Mix\Model\Field\Decorator\FontAwesomeDecorator;
use Symfony\Component\HttpFoundation\Request;

class Vitrine extends BaseModel
{
    const UPLOAD_DIR = 'uploads/vitrines/';
    protected $table = 'vitrines';

    static protected $fieldsI18n = [
        'titulo',
        'imagem',
        'imagem_mobile',
        'youtube_url',
        'url',
    ];

    /**
     * Tamanhos das imagens do model
     *
     * w = width
     * h = height
     *
     * @var array
     */
    protected $imageSizes = [];


    public function init()
    {

        $config = $this->application['config']->load('vitrines')->as_array();

        $this->text('titulo', 'Título')->mandatory()->maxLength(200);

        if(!empty($config['imageSizes'])) {
            $this->imageSizes = $config['imageSizes'];
        }

        if(!empty($config['imageSizes']['imagem'])) {
            $size = $config['imageSizes']['imagem'];

            $this
                ->image('imagem', 'Imagem', $size['w'], $size['h'], 'RC', self::UPLOAD_DIR)
                ->fileMaxSize(10, FileMaxSize::MB)
                ->imageMinWidth($size['w'])
                ->imageMinHeight($size['h'])
                ->setInfo(
                    "Largura de <strong>$size[w]</strong> pixels e Altura de <strong>$size[h]</strong> pixels."
                )
                ->mandatory();
        }

        if(!empty($config['imageSizes']['imagem_mobile'])) {
            $size = $config['imageSizes']['imagem_mobile'];

            $this
                ->image('imagem_mobile', 'Imagem Mobile', $size['w'], $size['h'], 'RC', self::UPLOAD_DIR)
                ->imageMinWidth($size['w'])
                ->imageMinHeight($size['h'])
                ->setInfo(
                    "Largura de <strong>$size[w]</strong> pixels e Altura de <strong>$size[h]</strong> pixels."
                );
        }

        $this
            ->url('youtube_url', 'Vídeo do Youtube')
            ->addDecorator(new FontAwesomeDecorator('youtube'));

        $this->url('url', 'Link');
        $this->yesNo('nova_aba', 'Abrir link em nova aba');
        $this->color('cor', 'Cor de fundo');

        $this
            ->dateTime('data_comeca', 'Começa')
            ->addDecorator(new FontAwesomeDecorator('calendar'));

        $this
            ->dateTime('data_termina', 'Termina')
            ->addDecorator(new FontAwesomeDecorator('calendar'));

        $this->boolean('ativa', 'Ativa', 1);
        $this->integer('ordem', 'Ordem');

        $this->timestamp('data_cadastro');
        $this->timestamp('data_atualizacao', true, false);
    }

    /**
     * Create the basic query builder logic
     *
     * @param string                            $alias
     * @param \Doctrine\DBAL\Query\QueryBuilder $queryBuilder
     *
     * @return \Doctrine\DBAL\Query\QueryBuilder
     */
    public static function createBaseQueryBuilder(\Doctrine\DBAL\Query\QueryBuilder $queryBuilder = null, $alias = null)
    {
        $queryBuilder = $queryBuilder ? $queryBuilder : self::getConnection()->createQueryBuilder();
        $alias = self::getAlias();

        $queryBuilder
            ->select(
                $alias . '.id',
                $alias . '.titulo',
                $alias . '.imagem',
                $alias . '.cor',
                $alias . '.data_comeca',
                $alias . '.data_termina',
                $alias . '.nova_aba',
                $alias . '.url',
                $alias . '.youtube_url',
                $alias . '.video',
                'IF(' . $alias . '.imagem_mobile IS NOT NULL, ' . $alias . '.imagem_mobile, ' . $alias . '.imagem) AS imagem_mobile'
            );

        // adds the from part only if its not defined elsewhere
        if(empty($queryBuilder->getQueryPart('from'))) {
            $queryBuilder->from(self::getTable(), $alias);
        }

        $queryBuilder
            ->where($alias . '.ativa=1')
            ->andWhere($alias . '.data_comeca IS NULL OR ' . $alias . '.data_comeca <= NOW()')
            ->andWhere($alias . '.data_termina IS NULL OR ' . $alias . '.data_termina >= NOW()')
            ->orderBy($alias . '.ordem ASC, ' . $alias . '.data_cadastro', 'DESC');

        return parent::createBaseQueryBuilder($queryBuilder, $alias);
    }

    public static function findAllParsed($modifyQueryCallback)
    {
        $query = static::buildFindQuery([], $modifyQueryCallback);

        if (
            class_exists('\Helper\LayoutHelper')
            && method_exists('\Helper\LayoutHelper', 'getScheme')
        ) {
            $scheme = \Helper\LayoutHelper::getScheme();
        } else {
            // not reliable
            $request = Request::createFromGlobals();
            $scheme = $request->getScheme();
        }

        $results = $query->execute()->fetchAll();

        if (empty($results)) {
            return [];
        }

        // fix values in json response
        foreach($results as $index => $result) {
            // resolve as absolute url
            if(!empty($results[$index]['imagem'])) {
                $results[$index]['imagem'] = \URL::site(
                    static::UPLOAD_DIR . $results[$index]['imagem'],
                    $scheme
                );
            }

            // resolve as absolute url
            if(!empty($results[$index]['imagem_mobile'])) {
                $results[$index]['imagem_mobile'] = \URL::site(
                    static::UPLOAD_DIR . $results[$index]['imagem_mobile'],
                    $scheme
                );
            }
        }

        return $results;
    }

    public static function findForHome($limit = null, $parsed = false)
    {
        if ($parsed) {
            return static::findAllParsed(function($query) use ($limit) {
                $query = self::createBaseQueryBuilder($query);

                if($limit) {
                    $query->setMaxResults($limit);
                }

                return $query;
            });
        } else {
            $query = self::createBaseQueryBuilder();

            if($limit) {
                $query->setMaxResults($limit);
            }

            return $query->execute()->fetchAll();
        }
    }

    public static function hasAny()
    {
        return boolval(
            static::createBaseQueryBuilder()
                ->select('count(*) total')
                ->execute()
                ->fetchColumn(0)
        );
    }
}
