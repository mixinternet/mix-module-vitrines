<?php
namespace Vitrines\Controller;

use Application\Controller\BaseController;
use Albums\Model\Album;
use Vitrines\Model\Vitrine;
use Symfony\Component\HttpFoundation\JsonResponse;

class VitrinesController extends BaseController
{
    public function indexAction()
    {
        try {
            $vitrines = Vitrine::findForHome(null, true);

            return new JsonResponse($vitrines);
        } catch(\Exception $exception) {
            $response = [
                'message' => $exception->getMessage()
            ];

            return new JsonResponse($response, 400);
        }
    }
}
