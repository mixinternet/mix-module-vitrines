<?php

use Helper\Format;
use Admin\Helper\AjaxField;
use Vitrines\Model\Vitrine;

return [
    'singular'        => 'Vitrine',
    'plural'          => 'Vitrines',
    'gender'          => 'f',
    'model'           => Vitrine::class,
    'active_menu'     => 'vitrines',
    'order_field'     => 'ordem',

    'imageSizes' => [
        'imagem' => [
            'w' => 1920,
            'h' => 720,
        ],
        'imagem_mobile' => [
            'w' => 750,
            'h' => 500
        ]
    ],

    'table'              => [
        'sort_drag_drop' => true,
        'fields'     => [
            'id'          => false,
            'imagem'      => '',
            'titulo'      => 'Título',
            'data_comeca' => 'Data de Agendamento',
            'data_termina' => false,
            'data_cadastro' => 'Data de cadastro',
            'ativa'       => 'Ativa',
        ],
        'hidden' => [],
        'order_by' => ['ordem ASC, data_cadastro', 'DESC'],
        'after_setup' => function (Mix\Table $table) {
            $table->setColumnAttributes('imagem', array('width' => '1%'));
            $table->setColumnAttributes('data_comeca', array('width' => '120px'));
            $table->setColumnAttributes('data_cadastro', array('width' => '100px'));
            $table->setColumnAttributes('ativa', array('width' => '1%'));
        },
        'filters'  => array(
            'imagem' => function ($value, $row) {
                $image = !empty($value)
                    ? \URL::site(Vitrine::UPLOAD_DIR . $value)
                    : 'https://placeholdit.co/i/100x50?bg=f6f6f6&fc=333333&text=?';

                return sprintf(
                    '<img src="%s" alt="%s" width="100px">',
                    $image,
                    $row['titulo']
                );
            },
            'ativa' => function($value, $row) {
                return AjaxField::boolean(
                    Vitrine::getTable(),
                    'ativa',
                    $value,
                    $row['id'],
                    'Desabilitar',
                    'Habilitar'
                );
            },
            'data_comeca' => function($value, $row) {
                $output = '';

                if(!empty($value)) {
                    $output .= sprintf('<div><b>De:</b> %s</div>', Format::_timestamp($value, '%d/%m/%Y'));
                }

                if(!empty($row['data_termina'])) {
                    $output .= sprintf('<div><b>Até:</b> %s</div>', Format::_timestamp($row['data_termina'], '%d/%m/%Y'));
                }

                return $output;
            },
            'data_cadastro' => function($value) {
                return Format::_timestamp($value, '%d/%m/%Y');
            },
        )
    ],
    'can_add' => true,
    'search' => [
        'titulo' => [
            'label' => 'Buscar por título',
            'type'  => 'text',
            'fields' => [
                'titulo'  => 'like'
            ]
        ],
        'data_comeca' => [
            'label' => 'Data do Evento',
            'type' => 'date',
            'fields' => [
                'data_comeca' => 'lte'
            ]
        ],
        'ativa' => [
            'label' => 'Ativa',
            'type' => 'options',
            'fields' => [
                'ativa' => 'exact'
            ],
            'options' => [
                'options' => [
                    0 => 'Não',
                    1 => 'Sim'
                ]
            ]
        ]
    ],

    'form_layout' => function (Mix\Html\FormBuilder $builder) {
        $builder->row(['titulo' => 12]);
        $builder->row(['cor' => 12]);
        $builder->row(['imagem' => 12]);
        $builder->row(['imagem_mobile' => 12]);
        $builder->row(['data_comeca' => 3, 'data_termina' => 3]);
        $builder->row(['url' => 6]);
        $builder->row(['youtube_url' => 6]);
        $builder->row(['nova_aba' => 3, 'ativa' => 3]);

        $layout = $builder->getLayout();
        return $layout;
    }
];
