import VitrineComponent from './VitrineComponent'

const VitrinePlugin = {
    install(Vue) {
        Vue.component(VitrineComponent.name, VitrineComponent)
    }
}

// Automatic installation if Vue has been added to the global scope.
if (typeof window !== 'undefined' && window.Vue) {
    window.Vue.use(VitrinePlugin)
}

export default VitrinePlugin
