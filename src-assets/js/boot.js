import Vue from 'vue'
import { create as axiosCreate } from 'axios'
import VitrineComponent from './VitrineComponent'

export default {
    init(componentId = 'VitrineCarousel', HTTP = null, config = {}) {
        if (document.getElementById(componentId)) {
            if (!HTTP) {
                HTTP = axiosCreate({
                    baseURL: WEBROOT || '/',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    },
                })
            }

            Vue.component(VitrineComponent.name, VitrineComponent)

            return new Vue({
                el: `#${componentId}`,

                data: {
                    items: [],
                    settings: {},
                    playerVars: {},
                    loading: false,
                    showArrows: true,
                    showBullets: false,

                    routeVitrines: null,
                },

                created() {
                    this.setup()
                    // this.fetchData()
                },
                render(h) {
                    return h('VitrineComponent', {
                        props: {
                            items: this.items,
                            loading: this.loading,
                            settings: this.settings,
                            playerVars: this.playerVars,
                            showArrows: this.showArrows,
                            showBullets: this.showBullets,
                        }
                    })
                },

                /**
                 * Override initial setup with HTML props
                 */
                beforeMount() {
                    if ('settings' in this.$el.attributes) {
                        this.settings = Object.assign(
                            this.settings,
                            JSON.parse(this.$el.attributes.settings.value)
                        )
                    }

                    if ('player-vars' in this.$el.attributes) {
                        this.playerVars = Object.assign(
                            this.playerVars,
                            JSON.parse(this.$el.attributes['player-vars'].value)
                        )
                    }

                    if ('show-arrows' in this.$el.attributes) {
                        this.showArrows = Object.assign(
                            this.showArrows,
                            JSON.parse(this.$el.attributes['show-arrows'].value)
                        )
                    }

                    if ('show-bullets' in this.$el.attributes) {
                        this.showBullets = Object.assign(
                            this.showBullets,
                            JSON.parse(this.$el.attributes['show-bullets'].value)
                        )
                    }

                    if ('data-route-vitrines' in this.$el.attributes) {
                        this.routeVitrines = this.$el.attributes['data-route-vitrines'].value

                        console.log('vitrine', this.routeVitrines)

                        this.fetchData()
                    }
                },

                methods: {
                    fetchData() {
                        if (!this.routeVitrines) {
                            throw new Error('Missing required routes')
                        }

                        this.loading = true

                        HTTP
                            .get(this.routeVitrines)
                            .then(response => {
                                this.loading = false
                                this.items = response.data ? response.data : [];
                            })
                            .catch(error => {
                                this.loading = false
                                console.log('Failed to fetch vitrine items', error)
                            })
                    },

                    /**
                     * Initial setup from init method
                     */
                    setup() {
                        if ('settings' in config) {
                            this.settings = config.settings;
                        }

                        if ('player-vars' in config) {
                            this.playerVars = config.playerVars;
                        }

                        if ('show-arrows' in config) {
                            this.showArrows = config.showArrows;
                        }

                        if ('show-bullets' in config) {
                            this.showBullets = config.showBullets;
                        }
                    },
                },
            })
        }
    },
}
