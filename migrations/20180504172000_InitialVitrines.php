<?php

use Phinx\Migration\AbstractMigration;

class InitialVitrines extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * @return void
     * @throws \RuntimeException
     * @throws \InvalidArgumentException
     */
    public function change()
    {
        $this
            ->table('vitrines', ['id' => false, 'primary_key' => ['id']])
            ->addColumn('id', 'integer', ['signed' => false, 'identity' => true])
            ->addColumn('titulo', 'string', [
                'limit' => 255,
                'null' => false
            ])
            ->addColumn('imagem', 'text', [
                'null' => false
            ])
            ->addColumn('imagem_mobile', 'text', [
                'default' => null,
                'null' => true
            ])
            ->addColumn('youtube_url', 'text', [
                'default' => null,
                'null' => true
            ])
            ->addColumn('video', 'text', [
                'default' => null,
                'null' => true
            ])
            ->addColumn('ordem', 'integer', [
                'default' => 99,
                'null' => true
            ])
            ->addColumn('cor', 'string', [
                'limit' => 7,
                'default' => null,
                'null' => true
            ])
            ->addColumn('nova_aba', 'boolean', [
                'default' => true,
                'null' => false
            ])
            ->addColumn('url', 'text', [
                'default' => null,
                'null' => true
            ])
            ->addColumn('ativa', 'boolean', [
                'default' => true,
                'null' => false
            ])
            ->addColumn('data_comeca', 'timestamp', [
                'null' => true
            ])
            ->addColumn('data_termina', 'timestamp', [
                'null' => true,
                'after' => 'data_comeca'
            ])
            ->addColumn('data_cadastro', 'timestamp', [
                'null' => true
            ])
            ->addColumn('data_atualizacao', 'timestamp', [
                'null' => true
            ])
            // Indexes
            ->addIndex(['id'], ['unique' => true])
            ->create();
    }
}
